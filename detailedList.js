// latest flight form spaceX api :
// https://api.spacexdata.com/v3/launches/latest
$(document).ready(function() {
  $.ajax({
    url:'https://api.spacexdata.com/v3/launches',
    type: 'GET',
    data: {
      sort: 'launch_date_utc',
      order: 'desc'
    },
    success: (success) => {
      success.forEach(element => {
        var missionName = element.mission_name;
        var launch_site = element.launch_site.site_name_long;
        var articleLink = element.links.article_link;
        var firstFlightImage = element.links.flickr_images[0];
        $('#spaceXTable').append(`
          <tr>
            <td class="isTitle">Lancement: ` + missionName + `<span class="lineStatus">+</span>
              <div class="clickedMore">
                <div id="leftElements" class="element">
                  <img id="frstMImg" src="` +  firstFlightImage + `" alt="This SpaceX Mission's First Image">
                </div>
                <div id="rightElements" class="element">
                  <p id="missionTitleName" class="titleFont">Mission Name :<br><span id="missionTitle">` + missionName + `</span></p>
                  <p id="missionSiteName" class="titleFont">Mission Launch Site:<br><span id="missionSite">` + launch_site + `</span></p>
                  
                  <span id="articleLink"><a id="articleLinkHref" href="` + articleLink + `">Lien vers l'article de la mission</a></span>

                </div>
              </div>
            </td>
          </tr>`
        );
      });
    },
    error: (error) => {
      console.log(error);
    }
  });

  $(document).click(function(event) {
    if (event.target.children[1].className == "clickedMore") {
      event.target.children[1].classList.add("openedTd");
    }
    else if (event.target.children[1].className === "clickedMore openedTd") {
      event.target.children[1].classList.remove('openedTd');
    }

    if (event.target.children[0].className === "lineStatus") {
      if (event.target.children[0].innerText === "+") {
        event.target.children[0].innerText='';
        event.target.children[0].innerText='-';
      }
      else {
        event.target.children[0].innerText='';
        event.target.children[0].innerText='+';
      } 
    }
  })
});
