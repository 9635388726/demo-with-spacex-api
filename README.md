# SpaceX API Demo (FRENCH)
[3 files : index.html/main.js/style.css]<br><br>
This is a little website, it <b>display lasts lauches</b> of <b>SpaceX</b>.<br>
C'est un petit site, il <b>affiche</b> les derniers décollages</b> de <b>SpaceX</b>.<br>

# Signidication des fichiers
ClickList.html
-
C'est une liste qui contient <b>tous les vols de spaceX</b>, avec des <b>informations supplémentaires</b> quand vous <b>cliquez sur (+)</b>.

DetailedList.html
-
C'est un petit site, il <b>affiche</b> les <b>3 derniers décollages</b> de <b>SpaceX</b>.<br>
Il vous permet d'afficher <b>d'autres décollages</b> (comme le 4e, le 5e, le 6e etc...) <b>chaques fois que vous cliquez le "+"</b>.

request.html
-
C'est un site qui <b>affiche</b> les <b>catégories</b> que vous <b>demandez</b> (Get, Post, Delete).